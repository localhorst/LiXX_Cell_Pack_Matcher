# LiXX Cell Pack Matcher

Tool for finding the best configuration in a LiXX Battery Pack. 
Matches capacity in parallel cell groups from a serial pack.

## Working
- Matches cells bases on capacity for varius Pack configuration. Set parallel and serial cell count respectively.
- Supports labels as identifier for cells.

## Not Working
- Clould be faster, 6S2P needs more than 10min to compute
- Support internal cell resistance matching
- Support bigger cell pool for a pack that is needed
